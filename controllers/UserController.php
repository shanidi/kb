<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException; 
use yii\filters\AccessControl;

 //  יוז לחוקים של האכיפה של ההרשאות

/**
 * UserController implements the CRUD actions for User model.
 */

class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
      return [
            'verbs' => [
                'class' => VerbFilter::className(),//כנראה שצריך יוז
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
              'access' => [
                'class' => AccessControl::className(),
                'only' => ['update','delete'],//החוקים חלים רק על הפונקציה עדכון שבקוד
                'rules' => [
                    [
                        'allow' => true,//לאפשר לעשות אפדייט
                        'actions' => ['update','delete'],//על איזו פונקציה מלמעלה מדברים
                        'roles' => ['manageUser'], // שם ההרשאה במסד הנתונים!!                       
                    ],
                ],
            ], 
        ];           
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->can('manageUser'))
        {
            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
          throw new ForbiddenHttpException( 'You are not allowed to perform this action.'); 

    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found


   
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      //  if (\Yii::$app->user->can('manageUser')) // רק אם יש הרשאה- דרך מס' 1 לאכיפה
        {
            $model = new User();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            return $this->render('create', [
                'model' => $model,
            ]);

        }
        //  throw new ForbiddenHttpException( 'You are not allowed to perform this action.'); הודעת השגיאה שאומרת שאין גישה- פעילה רם עם האיפ שלמעלה

    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
