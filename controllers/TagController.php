<?php

namespace app\controllers;
use Yii;
use app\models\Tag;
use app\models\TagSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
      return [
            'verbs' => [
                'class' => VerbFilter::className(),//כנראה שצריך יוז
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
              'access' => [
                'class' => AccessControl::className(),
                'only' => ['update','delete'],//החוקים חלים רק על הפונקציה עדכון שבקוד
                'rules' => [
                    [
                        'allow' => true,//לאפשר לעשות אפדייט
                        'actions' => ['update','delete'],//על איזו פונקציה מלמעלה מדברים
                        'roles' => ['manageTag'], // שם ההרשאה במסד הנתונים!!                       
                    ],
                
                ],
            ], 
        ];           
    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tag model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tag();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tag::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



public function actionList($query) //מקבל את התויים בקווארי מהיו אר אל- נוצר יו אר אל מאג'קס שנוצר כתוצאה מהקלדה
{
    $models = Tag::findAllByName($query); //ללכת למחלקה טאג ולהפעיל בה פונקציה בשפ פיינד אול ביי ניים שתקבל את התווים, לא חייב דולר, מקבלים רשימת תגים שיש בהם מה שהוקלד
    $items = [];

    foreach ($models as $model) { // מודל היא רשימת אובייקטים שנרצה לשלוף מהאובייקא רק את השם שלו, לכן נפעיל לולאה, מעתיקים את מודל
        $items[] = ['name' => $model->name];// ,מכניסים את השמות לאייטמס, מוסיפים איבר עם החץ הניים השני הוא שם התכונה במחלקה
    }
 // FORMAT_JSON- קבוע כי הוא באותיות גדולות- נושא ערך מסויים קבועעעעעעעעעעע- כללי ל איי
    Yii::$app->response->format = Response::FORMAT_JSON; // העתקה לג'ייסון

    return $items;
}

}
