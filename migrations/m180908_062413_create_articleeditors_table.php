<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articleeditors`.
 */
class m180908_062413_create_articleeditors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('articleeditors', [
            'article_id' => $this->integer(),
            'user_id' => $this->integer(),
            'PRIMARY KEY(article_id, user_id)'  
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('articleeditors');
    }
}
