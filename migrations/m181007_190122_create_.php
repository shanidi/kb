<?php

use yii\db\Migration;

/**
 * Class m181007_190122_create_
 */
class m181007_190122_create_ extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
            'subject' => $this->text(),
            'message' => $this->text(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181007_190122_create_ cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181007_190122_create_ cannot be reverted.\n";

        return false;
    }
    */
}
