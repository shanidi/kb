<?php

use yii\db\Migration;

/**
 * Class m181001_111326_init_rbac
 */
class m181001_111326_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            
//-------------------------------------תפקידים----------------------------------
$auth = Yii::$app->authManager;
$admin = $auth->createRole('admin');
$auth->add($admin);

$author = $auth->createRole('author');
$auth->add($author);
//-----------------------------הגדרת ילדים ובנים---------------------------------
$auth->addChild($admin, $author);
//-----------------------------------------------------הרשאות----------------------------------------------------

$deleteArticle = $auth->createPermission('deleteArticle'); //מנהל
$auth->add($deleteArticle);

$updateArticle = $auth->createPermission('updateArticle');  //עורך
$auth->add($updateArticle);   

$updateOwnArticle = $auth->createPermission('updateOwnArticle');//עורך עם חוק

$createArticle = $auth->createPermission('createArticle');  //עורך
$auth->add($createArticle); 

$manageUser = $auth->createPermission('manageUser'); // מנהל
$auth->add($manageUser);

$addCategory = $auth->createPermission('addCategory'); //מנהל
$auth->add($addCategory);

$manageTag = $auth->createPermission('manageTag'); // מנהל
$auth->add($manageTag);

//----------------------------------------קישור לקובץ חוקים---------------------------------------------------------------------------
$rule = new \app\rbac\aRule;
$auth->add($rule);
//-----------------------------------------חוקים-----------------------------------        
$updateOwnArticle->ruleName = $rule->name;                
$auth->add($updateOwnArticle);
//-------------------------------------שיוך תפקידים להרשאות------------------------    
$auth->addChild($admin, $deleteArticle);
$auth->addChild($admin, $updateArticle);
$auth->addChild($author, $updateOwnArticle);
$auth->addChild($author, $createArticle);
$auth->addChild($admin, $manageUser);
$auth->addChild($admin, $manageTag);
$auth->addChild($admin, $addCategory);
//-----------------------------קישור בין החוק עם הכובע לבלי הכובע---------------------
$auth->addChild($updateOwnArticle, $updateArticle); 

$this->insert('auth_assignment', [
    'item_name' => 'admin',
    'user_id' => '2',
]);

$this->insert('auth_assignment', [
    'item_name' => 'author',
    'user_id' => '1',
]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181001_111326_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181001_111326_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
