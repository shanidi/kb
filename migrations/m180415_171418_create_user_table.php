<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180415_171418_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    
       $this->createTable('user', [
           'id' => $this->primaryKey(),
           'name'=> $this->string(),
           'email' => $this->string(),
           'username' => $this->string()->unique(),
           'auth_key' => $this->string(),
           'password' => $this->string(),
           'created_at' => $this->timestamp(),
           'updated_at' => $this->timestamp(),
           'created_by' => $this->integer(),
           'updated_by' => $this->integer(),
       ]);

       $this->insert('user', [
        'name' => 'jack',
        'email' => 'jack@jack.jack',
        'username' => 'jack',
        'auth_key' => 'x_EU4ONmucQYkFnW7SGH8lKYnq8EHaJk',
        'password' => '$2y$13$pRlm.t3nksr8Y1XAxTMNuelq45sMfaECOZ2Vg9yZfCS2f1B.esYVu'


    ]);

    $this->insert('user', [
        'name' => 'alice' ,
        'email' =>'alice@alice.alice',
        'username' => 'alice',
        'auth_key' => 'Gda6fajQQmG9lUHZyxBbkRYwosIymahP',
        'password' => '$2y$13$RLThPKjkGrFLsgUSkKpxDe3huCIGLIq.fG6MqL4ODoYWFW3umi9DG'
    ]);
        $this->insert('user', [
        'name' => 'shani',
        'email' => 'shani@shani.shani',
        'username' =>'shani',
        'auth_key' => '7I_E6QS88GGWlonfuSdCNiTLtIiDTNGY' ,
        'password' => '$2y$13$D6Z3VLIquzehpztpmDoDf.SgTYblqDmMQ3wT7HLtk6OGZjcBy6are'
    ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
