<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article_users_assn`.
 */
class m180907_132407_create_article_users_assn_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('article_users_assn', [
            'article_id' => $this->integer(),
            'user_id' => $this->integer(),
            'PRIMARY KEY(article_id, user_id)'  
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('article_users_assn');
    }
}
