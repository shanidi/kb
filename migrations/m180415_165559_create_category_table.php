<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m180415_165559_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer()
        ]);

        $this->insert('category', [
            'name' => 'taxes'
        ]);
        $this->insert('category', [
            'name' => 'regulations'
        ]);
        $this->insert('category', [
            'name' => 'trade'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}
