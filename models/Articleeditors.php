<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articleeditors".
 *
 * @property int $article_id
 * @property int $user_id
 */
class Articleeditors extends \yii\db\ActiveRecord
{ public $articleeditors;
  
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articleeditors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['article_id', 'user_id'], 'integer'],
            [['article_id', 'user_id'], 'unique', 'targetAttribute' => ['article_id', 'user_id']],
        ];
    }
 
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article_id' => 'Article ID',
            'user_id' => 'User ID',
        ];
    }
  public function getUser(){
        return $this->hasMany(User::className(), ['id' => 'user_ids']); }
}
