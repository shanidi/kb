<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article;
use app\models\User;
use app\models\UserSearch;



/**
 *ArticleSearch represents the model behind the search form of `app\models\Article`.
 */
class ArticleSearch extends Article
{
    public $globalSearch;
    /**
     * @inheritdoc
     */

    public $tag;

    public function rules()
    {
        return [
            [['id', 'category_id', 'created_by', 'updated_by'], 'integer'],
            [['articleSearch','globalSearch','title', 'descriptin', 'body', 'created_at', 'updated_at','tag'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

      //  if (!$this->validate()) {
        if (!($this->load($params) && $this->validate())){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
  
     // grid filtering conditions
      /*     $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id ,
           'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,          
        ]); */
     
     // $query->andFilterWhere(['like', 'category_id', $this->category_id]);

        //Add tags condition

        $query->joinWith(['user']); //לצורך חיפוש לפי יוזר
        $dataProvider->sort->attributes['author_id'] = [ //לצורך חיפוש לפי יוזר
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['User.name' => SORT_ASC],
            'desc' => ['User.name' => SORT_DESC],
        ]; 

        $query->joinWith(['category']); //לצורך חיפוש לפי קטגוריה
        $dataProvider->sort->attributes['category_id'] = [ //לצורך חיפוש לפי קטגוריה
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['Category.name' => SORT_ASC],
            'desc' => ['Category.name' => SORT_DESC],
        ];

         $query->orFilterWhere(['like', 'title', $this->globalSearch])
           ->orFilterWhere(['like', 'descriptin', $this->globalSearch])
            ->orFilterWhere(['like', 'body', $this->globalSearch])
            ->orFilterWhere(['like', 'category.name', $this->globalSearch])
            ->orFilterWhere(['like', 'User.name', $this->globalSearch]); 
       
            $query->andFilterWhere(['like', 'category.name', $this->category_id]);
     
        /*    $query->andWhere(['or',
            ['like','title' , $this->globalSearch],
            ['like','descriptin', $this->globalSearch], 
            ]);*/
      
        if(!empty($this->tag))
        {
           $condition = Tag::find()
                    ->select('id')
                    ->where(['IN', 'name', $this->tag]);
            $query->joinWith('tags');
            $query->andWhere(['IN', 'tag_id', $condition]);

        } 
        return $dataProvider;
    }
    //------------------------------------------------------------------------------------------

}
