<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Tag".
 *
 * @property int $id
 * @property int $frequency
 * @property string $name
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['frequency'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'frequency' => 'Frequency',
            'name' => 'Name',
        ];
    }

    public static function findAllByName($name)
{
    return Tag::find()->where(['like','name',$name])->limit(50)->all(); //פונים לפונקציה פיינד ומגדירים תנאי, לייק- מחזיר אמת/שקר בהתאם להאם התווים נמצאים בסטרינג מסויים
}


    
}
