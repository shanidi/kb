<?php

namespace app\models;
use dosamigos\taggable\Taggable;
use Yii;
//////////////test////////////
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\models\Articleeditors;
use app\models\User;

use app\models\Comments;



//////////////////////////////

/**
 * This is the model class for table "Article".
 *
 * @property int $id
 * @property string $title
 * @property string $descriptin
 * @property string $body
 * @property int $category_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Article extends \yii\db\ActiveRecord
{
    

    public $articleEditorsNames;
    /////////////////////////////////////////////test////////////////////////

    ////////////////////////////////////////////////////////////////////////
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'required'],
            [['tagNames'],'safe'],
            [[ 'category_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'descriptin'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'descriptin' => 'Description',
            'body' => 'body of Article',
            'category_id' => 'Category',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'rating' => 'rating',
           
        ];
    }
    public function getCategory(){
        return $this->hasOne(Category::className(),['id'=>'category_id']);
    } 
 

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('article_tag_assn', ['article_id' => 'id']);
    }

    public function getArticleeditors()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('articleeditors', ['article_id' => 'id']);
    }

 
  /*  public function getEditors()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('article_users_assn', ['article_id' => 'id']);
    } */
    ///////////////////////test//////////////////////////////////////////////////////////
    public function behaviors()
    {
        return [
       
            BlameableBehavior::className(),
          
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    
                ],
                'value' => new Expression('NOW()'),
            ],
          'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ], 
            Taggable::className(),
                     
        ];
    }
//-------------------------כדי שיראו ערכים הגיוניים ולא מספרי איי די---------------------------------
    public function getcreatedbys() // נוצר על ידיי מי
    {
        return $this->hasOne(User::className(),['id'=>'created_by']); 
    } 
    public function getupdatedbys() // עודכן על ידיי
    {
    return $this->hasOne(User::className(),['id'=>'updated_by']); 
    }
  
    public function getUser()
    {
      return $this->hasOne(User::className(),['id'=>'created_by']); 
    }

 

//-----------------------------------------------------------------------------------------------------------



}
