<?php

namespace app\models;

namespace app\models;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;
/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $password_repeat
 * @property string $auth_key
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $password_repeat;
        public function behaviors() {
        return [
          BlameableBehavior::className(),
            
               [
            'class' => TimestampBehavior::className(),
            'createdAtAttribute' => 'created_at',
            'updatedAtAttribute' => 'updated_at',
            'value' => new Expression('NOW()'),
        ]
        ];
        } 
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return 
        [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['name', 'username', 'password', 'auth_key','email'], 'string', 'max' => 255],
            [['username','email'], 'unique'],
            [['email'], 'email'],
            [['password','password_repeat','name','username','email'], 'required'],
            ['password', 'compare', 'compareAttribute'=>'password_repeat'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'email' => 'email',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Crated By',
            'updated_by' => 'Updated By',
        ];
    }

//--------------------------
//------------------------------------------------------
//פונקציות של המחלקה שחייב לממש
 public static function findIdentityByAccessToken($token, $type = null)
    {
     //   return static::findOne(['access_token' => $token]);
    }
          public static function findIdentity($id) // אם אנחנו מחוברים ורוצים לקבל עוד פרטים על היוזר
    {
        return static::findOne($id);
    }
       public function validateAuthKey($authKey) // שלוש שווה- מאמת גם את סוג המשתנה, בדיקה יותר מחמירה
    {
       // return $this->auth_key === $authKey;
    }
      public function getAuthKey()
    {
       // return $this->auth_key;
    }
//-------------------------------------------------------------------------
    public function getId()
    {
        return $this->id;
    }
    public static function findByUsername($username)
    {
        return self::findOne(['username'=>$username]); // בודקים במסד נתונים אם יש לנו יוזרניים כמו שקיבלנו
    }

   public function validatePassword($password) 
    { // פונקציה שמשווה בין הסיסמה שהמשתמש מקליד לבין הסיסמה הקיימת המערכת כאשר הסיסמה במערכת היא מוצפנת. לכן נצפין את הסיסמה שמגיעה מהמשתמש ואז נשווה לסיסמה בדטא בייס
       return Yii::$app->security->validatePassword($password,$this->password);
     
    }
    

      public function beforeSave($insert)
     { // אם המחלקה היא אקטיב רקוד הפונקציה תפעל כל פען לפני שנשמור משהו
        if (parent::beforeSave($insert)) { // דורסים ומפעילים שובת נרצה שזה יעבוד רק אם הפונקציה המקורית פועלת
            if($this->isNewRecord)
            { //אם זה פעם ראשונה שמשתמש נכנס, איז ניו רקורד- תכונה של איי
            $this->auth_key= \Yii::$app->security->generateRandomString(); // נגדיר הוס קי על ידיי הכנסת מחרוזת אקראית לשדה
            }
            if($this->isAttributeChanged('password'))
            { // אם בעצם בוצע שינוינ בסיסמה, נועד לזה שלא נעשה האש על האש
                $this->password = \Yii::$app->security->generatePasswordHash($this->password);
            }
            return true;
        }
        return false;
    } 

}

