<?php

use yii\helpers\Html;
use yii\grid\GridView;
use ogheo\comments\widget\Comments;



/* @var $this yii\web\View */
/* @var $searchModel app\models\RecipesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Article';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
  
  <?php  if (\Yii::$app->user->can('createArticle')) { ?>
    <p class="">
        <?= Html::a('add Article', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
  <?php } ?> 

<?php echo \nerburish\masonryview\MasonryView::widget([
	'dataProvider' => $dataProvider,
	'itemView' => '_item',
	'clientOptions' => [
	  'gutterWidth' => 15,
    ],

	'cssFile' => [
		"@web/css/masonry-demo.css"		
	]
]) ?>



   <?php //echo Comments::widget(); ?>
</div>
