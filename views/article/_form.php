<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Category;
use dosamigos\selectize\SelectizeTextInput;
use kartik\select2\Select2;
use app\models\User;
use app\models\Article;
use kartik\rating\StarRating;




/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row ct">
    <div class="article-form">
      
        <div class="col-lg-12 panel panel-default bd">

            <?php $form = ActiveForm::begin(); ?>
            <div><h1 align="center"><?=Html::encode($this->title)?></h1></div>
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?> 

            <?=/*שקול לאקו*/ $form->field($model, 'descriptin')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
        <!--------------------------------------------בחירת טגים---------------------------------------------------------------->
            <?= $form->field($model, 'tagNames')->widget(SelectizeTextInput::className(), [
            // calls an action that returns a JSON object with matched
            // tags
            'name' => 'tags',
            // how do I populate this? Should query the Tag model get all the objects 
            // and put them in a an array in item 
            // https://www.yiiframework.com/forum/index.php/topic/74602-selectize-widget-for-yii-2/
            // https://hotexamples.com/examples/dosamigos.selectize/SelectizeDropDownList/-/php-selectizedropdownlist-class-examples.html
        //https://github.com/2amigos/yii2-taggable-behavior/issues/12
            //'items' => $items, 
            'loadUrl' => ['tag/list'],// -פונה לטאג ומפעיל שם אקשן בשם ליסט- אופן הפעולה ב איי
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'plugins' => ['remove_button'],
                'valueField' => 'name',
                'labelField' => 'name',
                'searchField' => ['name'],
                'create' => true,
            ],
            ])->hint('Use commas to separate tags') ?>
        <!---------------------------------------------הצגה של היוזרים--------------------------------------------------------------->

      <?php  /*  echo '<label class="control-label">Additional editors</label>' ?> 
        <?= Select2::widget([

                'name' => 'Articleeditors[user_id]',
                'value' => '', // initial value
                'data' => ArrayHelper::map(User::find()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Select your editors...', 'multiple' => true],
                'pluginOptions' => [
                    'maximumInputLength' => 10,
                    'allowClear' => true
                ],
                ]) */?>
        <!------------------------------------------------------------------------------------------------------------>

            <?= $form->field($model, 'category_id')->dropDownList(
                ArrayHelper::map(Category::find()->asArray()->all(), 'id','name') //Category::find()->asArray()  מביא את כל השדות ולכן נשתמש במאפ שיקח את האיי-די וניים 
            ) ?>

                <!-- $form->field($model, 'author_id')->dropDownList(
                ArrayHelper::map(User::find()->asArray()->all(), 'id','name') ) -->



            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
          </div>
    </div>
</div>
