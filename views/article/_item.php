
<?php


use yii\helpers\Html;
use yii\widgets\DetailView;
use ogheo\comments\widget\Comments;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\selectize\SelectizeTextInput;

use yii\grid\GridView;    ?>

<h3><?= $model->title ?></h3>
<p>by <?= $model->created_by ?></p>
<p><?= $model->body ?></p>


<p class="boot1">  <?= Html::a('View', ['view', 'id' => $model->id], ['class' => 'btn btn-primary']) ?></p>
<?php  if (\Yii::$app->user->can('updateArticle')) { ?>
    <p class="boot1">  <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?></p>
<?php } ?>
<?php  if (\Yii::$app->user->can('deleteArticle')) { ?>
    <p class="boot1">       <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?> </p>
<?php } ?> 
        



        
     