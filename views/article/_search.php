<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;



use yii\helpers\ArrayHelper;
use app\models\Category;
use dosamigos\selectize\SelectizeTextInput;
use kartik\select2\Select2;
use app\models\User;
use app\models\Article;

/* @var $this yii\web\View */
/* @var $model app\models\ArticleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    
    <div class= "mySeach">
    <?=  $form->field($model, 'globalSearch') ?>
    </div>
    <div class= "mySeach2">
    <?= $form->field($model, 'tag') ?>
    </div>
  

  

    <!-- <div class="col-lg-4">
    <?php /* echo $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map(Category::find()->asArray()->all(), 'id','name') //Category::find()->asArray()  מביא את כל השדות ולכן נשתמש במאפ שיקח את האיי-די וניים 
     )  */?>
     </div>-->



    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
    
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('reset', [''], ['class' => 'btn btn-default']) ?>
  
    </div>
    


    <?php ActiveForm::end(); ?>

</div>
