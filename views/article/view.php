<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use ogheo\comments\widget\Comments;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\selectize\SelectizeTextInput;




/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Article', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>
    


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'descriptin',
            'body:ntext',
           [ 'label' => 'created by',
				'format' => 'html',
				'value' => Html::a($model->createdbys->name, 
                 ['user/view', 'id' => $model->createdbys->id]), 
            ], 
      
    
        
            [ 'label' => 'Category',
                'format' => 'html',
                'value' => Html::a($model->category->name, 
                ['category/view', 'id' => $model->category->id]), 
            ],
        [
            'label' => 'Tags',
            'format' => 'html',
            'value' => $tags
        ],

         
        ],
    ]) ?>


    <?php echo Comments::widget(); ?>




</div>
