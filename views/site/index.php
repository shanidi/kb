<?php

use ogheo\comments\widget\Comments;


/* @var $this yii\web\View */

$this->title = 'Economy Magazine';
?>





<div class="site-index">

    <div class="jumbotron">
        <h1>Economy Magazine</h1>
        <h2>Everything new and important in the financial world</h2> 
        <p class="lead"></p>

      <!--  <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p> -->
    </div>

    <div class="body-content">

        <div class="row">
            <a href="http://localhost/kb/web/index.php/article/index?ArticleSearch%5BglobalSearch%5D=taxes">
                <div class="col-lg-4 maintext" >
                    <h2>taxes</h2> 
                    <p></p>
                    <img class="mainphoto" src="https://image.flaticon.com/icons/svg/1143/1143867.svg" alt="tax" >

                <!--   <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p> -->
                </div>
            </a>
            <a href="http://localhost/kb/web/index.php/article/index?ArticleSearch%5BglobalSearch%5D=regulations">
                <div class="col-lg-4 maintext">
                    <h2>Regulations</h2>

                <!--  <p>כשיש זמן, שרוצים לפנק או להרשים</p> -->
                    <img  class="mainphoto"    src="https://image.flaticon.com/icons/svg/1024/1024783.svg" alt="Regulations" >

                <!-- <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p> -->
                </div>
             </a>
             <a href="http://localhost/kb/web/index.php/article/index?ArticleSearch%5BglobalSearch%5D=trade">
                <div class="col-lg-4 maintext">
                    <h2>trade</h2>

                <!--    <p>החלק שפשוט אי אפשר בלעדיו</p> -->
                    <img  class="mainphoto"    src="https://image.flaticon.com/icons/svg/1024/1024791.svg" alt="trade" >

                <!-- <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>  -->
                </div>
            </a>
        </div>


    </div>
</div>