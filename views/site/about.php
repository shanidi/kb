<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
<h2>Economy Magazine</h2>
    <p class="about">
    In this magazine you will find everything new, interesting and relevant to the world of economy.
    If you would like to expand your knowledge of economics, you are welcome to read our articles from time to time.
    </p>

    <!--<code><?= __FILE__ ?></code> -->
</div>