<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    
<div class="row ct">
    <div class="col-lg-4"></div>
    <div  class="col-lg-4 panel panel-default bd">
    <div><h1 ><?=Html::encode($this->title)?></h1></div>
        <p>Please fill out the following fields to login:</p>
        <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                //'layout' => 'horizontal',
                //'fieldConfig' => [
                   // 'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    //'labelOptions' => ['class' => 'col-lg-1 control-label'],
               // ],
            ]); ?>
                
                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) //משתנים שנקלטים לתוך מודל ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox([
                // 'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                ]) ?>
<!--
                <div class="form-group">
                  <div class="col-lg-offset-1 col-lg-11"> -->
                        <?= Html::submitButton('Login', ['class' => 'col-lg-12 btn btn-primary', 'name' => 'login-button']) ?>
                        <br></br>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>


    
    
    </div>
</div>
    


    

   


</div>